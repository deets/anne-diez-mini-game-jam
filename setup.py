from setuptools import setup, find_packages

setup(
    name="FishTank",
    version="0.1",
    description="Fish Tank",
    author="Diez B. Roggisch",
    author_email="deets@web.de",
    entry_points= {
        'console_scripts' : [
            'fish-tank = fishtank.main:main',
            ]},
    install_requires = [
    ],
    zip_safe=True,
    packages=find_packages(),
    )

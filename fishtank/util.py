import os
from json import loads
from functools import partial

import pyglet
from Box2D import b2PolygonShape, b2Vec2, b2Transform, b2TestOverlap


def compose_func(f, times):
    def _f(arg, times):
        if times == 0:
            return arg
        return _f(f(arg), times-1)
    return partial(_f, times=times)


loader = pyglet.resource.Loader(os.path.dirname(__file__))



def load_with_anchor(name, anchor_info):
    img = loader.image(name)
    if name in anchor_info:
        anchor = anchor_info[name]
    else:
        anchor = img.width // 2, img.height // 2

    img.anchor_x, img.anchor_y = anchor
    return img


GAME_DATA = None

def load_game_data():
    global GAME_DATA
    if GAME_DATA is None:
        name = os.path.join(os.path.dirname(__file__), "game-data.json")
        assert os.path.exists(name)
        content = open(name).read()
        GAME_DATA = loads(content)
    return GAME_DATA


class Mask(object):


    def __init__(self, a, anchor):
        self.a = a
        self.anchor = anchor



def mask_from_image(img):
    if hasattr(img, "image"):
        img = img.image

    idata = img.get_image_data()
    alpha = idata.get_data("A", img.width)
    mask = greater(frombuffer(alpha, dtype=uint8).reshape((img.height, img.width)), 0)
    anchor = img.anchor_x, img.anchor_y
    return Mask(mask, anchor)



class CollisionMixin(object):


    def bounds(self):
        texture = self.sprite._texture
        w, h = texture.width, texture.height
        ax, ay = texture.anchor_x, texture.anchor_y
        scale = self.sprite.scale
        x, y = self.pos
        s = b2PolygonShape()
        s.SetAsBox(w * scale, h * scale, b2Vec2(ax * scale, ay * scale), 0)
        tf = b2Transform()
        tf.position = self.pos
        tf.angle = 0
        return s, tf



    def collides(self, other):
        if self.sprite is None or other.sprite is None:
            return False

        ab, atf = self.bounds()
        bb, btf = other.bounds()
        return b2TestOverlap(ab, 0, bb, 0, atf, btf)

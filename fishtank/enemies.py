import os
from random import random
import pyglet
from pyglet.image import Animation, AnimationFrame

from .util import load_with_anchor, load_game_data, CollisionMixin



class Enemy(CollisionMixin):

    def __init__(self, batch, group, name_base):
        game_data = load_game_data()
        base = os.path.dirname(__file__)
        right_frames = []
        for name in os.listdir(base):
            if name_base in name:
                frame = AnimationFrame(load_with_anchor(name, {}), .3)
                right_frames.append(frame)

        assert right_frames
        self.right_animation = Animation(right_frames)

        self.sprite = None
        self.reset()

        def create_sprite():
            sprite = pyglet.sprite.Sprite(self.right_animation, x= 0, y = 0, batch = batch, group=group)
            sprite.scale = game_data[name_base]["scale"]

            speed, speed_offset = game_data[name_base]["speed"]
            self.pos = -100, random() * 300 + 50
            self.speed = random() * speed + speed_offset, 0
            return sprite

        self.create_sprite = create_sprite


    def reset(self):
        if self.sprite is not None:
            self.sprite.delete()
        self.sprite = None
        self.pos = 0, 0
        self.speed = 0, 0
        self.pause = random() * 3 + 2


    def animate(self, dt):
        if self.sprite is None:
            self.pause -= dt
            if self.pause <= 0:
                self.sprite = self.create_sprite()

        x, y = self.pos

        if x >= 1200:
            self.reset()
        else:
            sx, sy = self.speed
            self.pos = x + sx, y + sy
            if self.sprite:
                self.sprite.position = self.pos

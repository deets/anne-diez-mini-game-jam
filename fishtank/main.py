import pyglet

from pyglet.media import Player


from pyglet.window import key
from pyglet.gl import (
    glPushMatrix,
    glPopMatrix,
    glTranslatef,
    glRotatef,
    )

from .actor import Fish
from .background import Waves, Sky
from .enemies import Enemy
from .util import loader

window = pyglet.window.Window(fullscreen=True)


batch = pyglet.graphics.Batch()

sky_group = pyglet.graphics.OrderedGroup(0)
bg_group = pyglet.graphics.OrderedGroup(1)
mg_group = pyglet.graphics.OrderedGroup(2)
enemy_group = pyglet.graphics.OrderedGroup(3)
fg_group = pyglet.graphics.OrderedGroup(4)
debug_group = None #pyglet.graphics.OrderedGroup(5)

fish = Fish(batch, fg_group, debug_group=debug_group, pos=(100, 540))
waves = Waves(batch, bg_group, mg_group)
sky = Sky(batch, sky_group, bg_group)

enemies = [Enemy(batch, enemy_group, name) for name in ["jellyfish", "shark"]]

game_objects = [fish, waves, sky] + enemies

background_song = loader.media("underwater.mp3", streaming=False)


@window.event
def on_key_press(symbol, modifiers):
    if symbol in [key.UP, key.DOWN, key.LEFT, key.RIGHT]:
        fish.move(symbol, pressed=True)


@window.event
def on_key_release(symbol, modifiers):
    if symbol in [key.UP, key.DOWN, key.LEFT, key.RIGHT]:
        fish.move(symbol, pressed=False)



@window.event
def on_draw():
    window.clear()
    glPushMatrix()
    hh = window.height / 2
    hw = window.width / 2
    glTranslatef(hw, hh, 0)
    glRotatef(fish.rotation, 0, 0, 1)
    glTranslatef(-fish.pos[0], -fish.pos[1], 0)
    batch.draw()
    glPopMatrix()


def update(dt):
    for enemy in enemies:
        if enemy.collides(fish):
            print "fish dead"
            fish.dead()


    for obj in game_objects:
        obj.animate(dt)



def main():
    pyglet.clock.schedule_interval(update, 1.0 / 30.0)
    player = Player()
    player.queue(background_song)
    player.eos_action = player.EOS_LOOP
    player.play()
    pyglet.app.run()

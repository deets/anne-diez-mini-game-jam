from random import random
from math import cos
import pyglet

from .util import loader, load_with_anchor


class Waves(object):

    def __init__(self, batch, bg_group, mg_group):
        bg_wave = loader.image("waves_dark.png")
        fg_wave = loader.image("waves_light.png")
        self.fg_pos = (-fg_wave.width // 2, 0)
        self.bg_pos = (-bg_wave.width // 2, 0)
        self.time = 0
        self.sprites = [
            pyglet.sprite.Sprite(bg_wave, batch=batch, group=bg_group),
            pyglet.sprite.Sprite(fg_wave, batch=batch, group=mg_group),           ]


    def animate(self, dt):
        self.time += dt
        offset = cos(self.time) * 30
        self.sprites[0].position = self.bg_pos[0] + offset, self.bg_pos[1]
        self.sprites[1].position = self.fg_pos


class Cloud(object):


    def __init__(self, y, batch, group):
        cloud_png = loader.image("cloud.png")
        self.sprite = pyglet.sprite.Sprite(
            cloud_png, batch=batch, group=group,
            )
        self.y_base = y - cloud_png.height
        self.pos = random() * 400, self.y_base - random() * 100
        self.speed = 20 + random() * 60


    def animate(self, dt):
        x, y = self.pos
        x -= dt * self.speed
        if x < -400:
            x = 1200
            y = self.y_base - random() * 100
        self.sprite.position = self.pos = x, y


class Sky(object):

    def __init__(self, batch, sky_group, bg_group):
        y = 300
        sky_img = loader.image("sky.png")
        self.sky_sprite = pyglet.sprite.Sprite(
            sky_img, x=-sky_img.width // 2, y=y, batch=batch, group=sky_group,
            )

        self.clouds = [Cloud(y + sky_img.height, batch, bg_group) for _ in xrange(2)]

    def animate(self, dt):
        for cloud in self.clouds:
            cloud.animate(dt)

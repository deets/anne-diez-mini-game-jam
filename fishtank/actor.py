import os
from math import sqrt, acos, asin, pi, atan2
import pyglet
from pyglet.window import key
from pyglet.image import Animation, AnimationFrame

from .util import compose_func, load_with_anchor, CollisionMixin, loader

class Fish(CollisionMixin):

    DIVE_SPEED = 15
    DRAG = .95
    GRAVITY = 10.0

    WINDOW = (40, 10, 800, 500)

    def __init__(self, batch, group, top_speed=10, acceleration=50.0, waterline=550.0, debug_group=None, pos=None):

        anchor_info = {}

        frames = [AnimationFrame(load_with_anchor(name, anchor_info), .3) for name in ["fish_swim_1.png", "fish_swim_2.png"]]
        right_animation = Animation(frames)
        frames = [AnimationFrame(load_with_anchor(name, anchor_info), .3) for name in ["fish_swim_1_left.png", "fish_swim_2_left.png"]]
        left_animation = Animation(frames)

        self.images = {
            "swimming" : {
                "right" : right_animation,
                "left"  : left_animation,
                },
            "jumping" : {
                "right" : load_with_anchor("fish_jump_1.png", anchor_info),
                "left"  : load_with_anchor("fish_jump_1_left.png", anchor_info)
                },
            "dead" : {
                "right" : load_with_anchor("fish_dead.png", anchor_info),
                "left"  : load_with_anchor("fish_dead.png", anchor_info),
                }
           }

        self.direction = "right"
        self.mode = "swimming"

        self._pos = pos
        self.rotation = 0

        self.control_keys = {
            key.LEFT : False,
            key.UP : False,
            key.RIGHT : False,
            key.DOWN : False,
            }
        self.sprite = pyglet.sprite.Sprite(self.images[self.mode][self.direction], x= 0, y = 0, batch = batch, group=group)
        self.sprite.scale = .3
        self.speed = (0.0, 0.0)
        self.top_speed = top_speed
        self.acceleration = acceleration
        self.waterline = waterline

        self.debug_sprite = None
        if debug_group is not None:
            self.debug_sprite = pyglet.sprite.Sprite(load_with_anchor("red-dot.png", anchor_info), batch=batch, group=debug_group)

        self.splash = loader.media("water-splash.mp3", streaming=False)


    def animate(self, dt):
        old_direction = self.direction, self.mode
        x, y = self.pos
        sx, sy = self.speed

        if self.mode != "dead":
            new_mode = "jumping" if y >= self.waterline else "swimming"
            if new_mode != self.mode:
                self.splash.play()
            self.mode = new_mode


        if self.mode == "swimming":
            for k, pressed in self.control_keys.iteritems():
                if not pressed:
                    continue
                if k == key.UP:
                    sy += dt * self.acceleration
                if k == key.DOWN:
                    sy -= dt * self.acceleration

                if k == key.LEFT:
                    sx -= dt * self.acceleration
                if k == key.RIGHT:
                    sx += dt * self.acceleration

            sx, sy = sx * self.DRAG, sy * self.DRAG
            self.speed = sx, sy
            self.direction = "left" if sx < 0 else "right"
            self.rotation = (atan2(sy, -sx) / pi * 180.0) + (180 if self.direction == "right" else 0)

        elif self.mode == "jumping":
            sy -= self.GRAVITY * dt
            self.speed = sx, sy
            self.rotation = (atan2(sy, -sx) / pi * 180.0) - 90.0


        else: # we're dead..
            if y >= self.waterline:
                self.speed = 0, 0
            else:
                self.speed = 0, 10


        self.pos = x + sx, y + sy

        self.sprite.position = self.pos
        self.sprite.rotation = self.rotation if self.mode != "dead" else 0

        if old_direction != (self.direction, self.mode):
            self.sprite.image = self.images[self.mode][self.direction]

        if self.debug_sprite:
            self.debug_sprite.position = self.pos


    def move(self, symbol, pressed):
        self.control_keys[symbol] = pressed


    @property
    def pos(self):
        return self._pos


    @pos.setter
    def pos(self, (x, y)):
        left, bottom, right, top = self.WINDOW
        x = left + ((x - left) % (right - left))
        if y <= bottom:
            y = bottom
            self.speed = self.speed[0], 0
        self._pos = x, y


    def dead(self):
        self.mode = "dead"
        self.rotation = 180.0
        self.sprite.image = self.images[self.mode][self.direction]
